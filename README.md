# XOR

A utility for xoring together strings and files.

## Installation

The xor program requires [LuaJIT](https://luajit.org/) to run. To decode base64-strings, you will also need to install [basexx](https://github.com/aiq/basexx).

## Usage

```
xor [OPTIONS] SOURCE...
```

Xor will read strings from various "sources" (files or strings passed directly on the command line). By default it assumes each source argument is a filename. This can be controlled via a number of flags, each of which affects all sources until
the next flag:
* `-b` interprets sources as base64-encoded strings
* `-f` interprets sources as filenames to read from. This is the default. Pass `-` to read from stdin
* `-s` interprets sources as raw strings
* `-x` interprets sources as hex-encoded strings

Example:
```
xor - -s "normal string" "another string" -x 68657820656E636F64656420737472696E67
```

This xors together the contents of stdin with two raw strings and a hex-encoded string (i.e. the last strings is hex-decoded first before xoring).

Strings from all sources are automatically repeated to match the length of the longest string.

For more information, see `--help`.
